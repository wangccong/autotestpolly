#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : comm_driver.py
@Time   : 2021/11/22 15:53
@IDE    : PyCharm
@Introduction:存放浏览器对象
"""
from selenium import webdriver
from config.project_config import DEF_BROWSER_TYPE,HEADLESS_FLAG,IMPLICITLY_WAIT
"""
1、单例模式
    日志
    配置
    测试时打开单个浏览器
    音乐播放器
"""
class Single(object):
    _instance = None  # 类变量，存储实例
    def __new__(cls, *args, **kwargs):
        if cls._instance is None:  # 如果没有实例化过
            cls._instance = super().__new__(cls)
        return cls._instance
class CommDriver(Single):
    """
    浏览器对象类
    """
    driver = None  # 用来控制产生一个浏览器
    def get_driver(self,
                   browser_type=DEF_BROWSER_TYPE,
                   headless_flag=HEADLESS_FLAG):
        """

        @param browser_type: 浏览器类型，默认DEF_BROWSER_TYPE
        @param headless_flag: 是否有头，False 有头，True 无头浏览器
        @return:返回浏览器实例
        """
        if self.driver is None:
            if not headless_flag:
                if browser_type =="chrome":
                    self.driver = webdriver.Chrome()
                elif browser_type =="firefox":
                    self.driver = webdriver.Firefox()
                elif browser_type == "IE":
                    self.driver = webdriver.Ie()
                else:
                    raise Exception(f'{browser_type}暂不支持',)
            else:
                if browser_type =="chrome":
                    self.option = webdriver.ChromeOptions()
                    self.option.add_argument('headless')
                    self.driver = webdriver.Chrome(options=self.option)
                elif browser_type =="firefox":
                    self.option = webdriver.FirefoxOptions()
                    self.option.add_argument('--headless')
                    self.driver = webdriver.Firefox(options=self.option)
                elif browser_type == "IE":
                    self.option = webdriver.IeOptions()
                    self.option.add_argument('headless')
                    self.driver = webdriver.Ie(options=self.option)
                else:
                    raise Exception(f'{browser_type}暂不支持',)
        self.driver.maximize_window()
        self.driver.implicitly_wait(IMPLICITLY_WAIT)
        return self.driver
if __name__ == '__main__':
    test_driver = CommDriver().get_driver()
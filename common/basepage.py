#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : basepage.py
@Time   : 2021/11/22 18:20
@IDE    : PyCharm
@Introduction:基类
"""
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from time import strftime
import warnings

from common.comm_driver import CommDriver
from utils.handle_path import screenshots_path, common_path
from utils.handle_log import log
from utils.handle_yaml import get_yaml_data

class BasePage:
    """
    打开浏览器
    输入网址
    定位元素
    输入文本
    清空文本
    点击元素
    等待后点击元素
    等待后获取文本
    鼠标双击事件
    """
    def __init__(self):
        self.driver = CommDriver().get_driver()
        self.locators = get_yaml_data(f'{common_path}elements.yaml')[self.__class__.__name__]
        for ele_name, locator in self.locators.items():
            setattr(self, ele_name, locator)  # setattr设置self实例，属性ele_name的值是locator

    def open_url(self, url):  # 二次封装实现方法，提供更多的拓展
        self.driver.get(url)

    def get_element(self, locator):
        """
        定位元素
        异常保护，如果定位不到元素可以截图、日志
        @param locator: 定位器 如 (By.ID,'username') ['id','username']
        @return: 返回元素
        """
        # type,value = locator
        # if type == 'css':
        #     element = self.driver.find_element_by_css_selector(value)
        # elif type == 'xpath':
        #     element = self.driver.find_element_by_xpath(value)
        # else:
        #     element = self.driver.find_element(*locator)
        try:
            element = self.driver.find_element(*locator)
            return element
        except Exception as e:
            self.save_screenshot()
            self.get_log_file(e)

    def get_elements(self, locator):
        return self.driver.find_elements(*locator)

    def input_text_old(self, locator, text):
        """
        在元素上输入内容
        @param locator: 定位器
        @param text: 输入内容
        @return: None
        """
        warnings.warn("input_text_old is deprecated. Please use input_text instead", DeprecationWarning, stacklevel=2,)
        self.get_element(locator).send_keys(text)

    def input_text(self, locator, text, append=False):
        """
        清空原内容后再输入内容
        保留原内容追加输入内容
        @param locator: 定位器
        @param text: 输入内容
        @param append: True是追加输入，Flase是清空后输入
        @return:
        """
        ele = self.get_element(locator)
        if not append:
            # self.get_element(locator).clear()
            # clear()方法失效，的替换方案
            '-----方案一，通过双击，然后send_keys(text)。缺点有时双击不一定选中所有内容，可能存在无法清除的风险-----'
            # self.double_click_element(locator)
            '-----方案二，通过键盘的快捷键进行全选，然后删除-----'
            ele.send_keys(Keys.CONTROL, "a")
            ele.send_keys(Keys.DELETE)
            # 输入内容
            ele.send_keys(text)
        else:
            ele.send_keys(text)

    def clear_text(self, locator):
        """
        清空原内容
        @param locator: 定位器
        @return: None
        """
        self.get_element(locator).clear()

    def click_element(self, locator):
        """
        点击元素
        @param locator: 定位器
        @return: None
        """
        self.get_element(locator).click()

    def get_element_text(self, locator):
        """
        获取元素的文本
        :param locator: 定位器
        :return:
        """
        return self.get_element(locator).text

    def get_elements_text(self, locator):
        # 遍历得到每个元素的text
        return [ele.text for ele in self.get_elements(locator)]

    def wait_click_element(self, locator):
        # TODO 封装这个函数，设置超时时间和轮询时间
        # if WebDriverWait(self,5,0.5).until(EC.element_to_be_selected(locator)):
        # presence_of_element_located
        wait_ele = WebDriverWait(self.driver, 5, 0.5).until(
            EC.visibility_of_element_located(locator))
        wait_ele.click()

    def wait_get_element_text(self, locator):
        """
        获取元素的文本
        @param locator: 定位器
        @return:
        """
        return WebDriverWait(self.driver, 5, 0.5).until(
            EC.visibility_of_element_located(locator)).text

    def double_click_element(self, locator):
        """
        双击操作
        @param locator: 定位器
        @return: None
        """
        # 定位到元素
        input_box = self.get_element(locator)
        # 执行鼠标事件的双击
        ActionChains(self.driver).double_click(input_box).perform()

    def save_screenshot(self, desc=None):
        current_time = strftime('%Y%m%d')
        self.driver.save_screenshot(f'{screenshots_path}{desc}定位不到{current_time}.png')
        # 方式二 : 写到文件中
        # log_time = strftime('%Y%m%d')  #再控制下追加写入
        # logging.basicConfig(filename=f'{logs_path}{current_time}.log')
        # logging.error(f'{desc}无法定位')
        # 方式三：封装logging

    def get_log_file(self, desc=None):
        log.error(f'{desc}无法定位')

# if __name__ == '__main__':
    # test_page = BasePage()
    # test_page.open_url(POLLY_URL)
    # test_page.input_text(['css selector', "input[name='username']"], "macro")
    # test_page.input_text(['css selector', "input[name='password']"], "macro123456")
    # test_page.click_element(['xpath',"//span[contains(text(),'登录')]"])
    # if test_page.wait_get_element_text(['xpath','//*[text()="首页"]']) == '首页':
    #     test_page.click_element(['xpath', '//img'])
    #     test_page.wait_click_element(['xpath', "//span[text()='退出']"])
    # class ProductListPage(BasePage):
    #     pass
    # mp = ProductListPage()
    # print(mp.all_product_brands)
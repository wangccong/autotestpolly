#!/usr/bin/env python
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : project_config.py
@Time   : 2021/11/22 17:08
@IDE    : PyCharm
@Introduction:项目配置文件
"""
# BROWER CONFIG
DEF_BROWSER_TYPE = 'chrome'  # 默认浏览器Chrome
HEADLESS_FLAG = False  # False 有头，True 无头浏览器
IMPLICITLY_WAIT = 5  # 默认隐式等待时间
POLLY_URL = 'http://192.168.1.6/'
HOME_URL = 'http://192.168.1.6/#/home'
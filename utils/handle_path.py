#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : handle_path.py
@Time   : 2021/11/23 20:35
@IDE    : PyCharm
@Introduction:
"""
import os
project_path = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]
common_path = os.path.join(project_path,'common\\')
config_path = os.path.join(project_path,'config\\')
test_datas_path = os.path.join(project_path,'test_datas\\')
exports_path = os.path.join(project_path,'exports\\')
logs_path = os.path.join(exports_path,'logs\\')
screenshots_path = os.path.join(exports_path,'screenshots\\')
reports_path = os.path.join(exports_path,'reports\\')


if __name__ == '__main__':
    print(common_path)
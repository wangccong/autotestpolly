#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : handle_rand_str.py
@Time   : 2021/11/25 17:55
@IDE    : PyCharm
@Introduction:
"""
import random
from string import ascii_letters,digits
def get_rand_str(length):
    return ''.join(random.sample(ascii_letters+digits,length))
if __name__ == '__main__':
    print(ascii_letters)
    print(digits)
    print(get_rand_str(5))
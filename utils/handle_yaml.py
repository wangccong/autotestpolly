#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : handle_yaml.py
@Time   : 2021/11/23 20:25
@IDE    : PyCharm
@Introduction:
"""
import yaml
def get_yaml_data(yaml_file):
    with open(yaml_file,encoding='utf-8') as f:
        return yaml.safe_load(f.read())
if __name__ == '__main__':
    print(get_yaml_data(f'../test_datas/login_failed_data.yaml'))
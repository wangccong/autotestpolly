#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : test_login.py
@Time   : 2021/11/23 13:35
@IDE    : PyCharm
@Introduction:登录模块测试用例
"""
import pytest
import allure
import os
from time import sleep
from PageObjects.LoginPage import LoginPage
from utils.handle_yaml import get_yaml_data
from utils.handle_path import test_datas_path,reports_path
from config.project_config import HOME_URL

@allure.epic('史诗：mall-admin')
@allure.feature('用户模块')
@allure.story('登录')
class Test_Login():
    @pytest.mark.parametrize('username,password,locator,expected',get_yaml_data(f'{test_datas_path}login_001_data.yaml'))
    def test_login_001(self,username,password,locator,expected):
        # 登录
        test_loginpage = LoginPage()
        test_loginpage.open_loginpage()
        test_loginpage.login_polly(username, password)
        # 断言 测试思想的体现
        # 首页出现另一个首页元素
        # 判断这个首页元素的文本是"首页"
        assert test_loginpage.wait_get_element_text(locator) == expected
        if test_loginpage.driver.current_url == HOME_URL:
            # sleep(5)
            test_loginpage.click_element(['xpath', '//img'])
            # sleep(5)
            test_loginpage.wait_click_element(['xpath', "//span[text()='退出']"])
    @pytest.mark.parametrize('username,password,locator,expected',get_yaml_data(f'{test_datas_path}login_success_data.yaml'))
    def test_login_success(self,username,password,locator,expected):
        """

        @param username: 用户名
        @param password: 密码
        @return:
        """
        test_loginpage = LoginPage()
        test_loginpage.open_loginpage()
        test_loginpage.login_polly(username, password)
        assert test_loginpage.wait_get_element_text(locator) == expected
        if test_loginpage.driver.current_url == HOME_URL:
            # sleep(5)
            test_loginpage.click_element(['xpath', '//img'])
            # sleep(5)
            test_loginpage.wait_click_element(['xpath', "//span[text()='退出']"])
    @pytest.mark.parametrize('username,password,locator,expected',get_yaml_data(f'{test_datas_path}login_failed_data.yaml'))
    def test_login_failed(self,username,password,locator,expected):
        """
        登陆失败的测试用例
        1、用户名输入错误，密码正确 预期出现提示"用户名或密码错误"
        2、密码输入错误           预期出现提示"密码不正确"
        3、密码太短              预期出现提示"密码不能小于三位"
        输入数据不同，判断的依据不同（元素不同，内容不同）
        username,password，(元素定位器),预期结果
        YAML
        @return:
        """
        test_loginpage = LoginPage()
        test_loginpage.open_loginpage()
        test_loginpage.login_polly(username, password)
        assert test_loginpage.wait_get_element_text(locator) == expected
    '''
    BLOCKER = 'blocker'
    CRITICAL = 'critical'
    NORMAL = 'normal'
    MINOR = 'minor'
    TRIVIAL = 'trivial'
    '''
    @allure.severity('critical')  # 登录功能是其他模块的依赖，可以用pytest-dependency
    @pytest.mark.parametrize('casetitle,username,password,locator,expected',get_yaml_data(f'{test_datas_path}login_all_data.yaml'))
    @allure.title('{casetitle}')
    def test_login_all(self,casetitle,username,password,locator,expected):
        """

        username,password，(元素定位器),预期结果
        YAML
        @return:
        """
        with allure.step('打开网页'):
            test_loginpage = LoginPage()
            test_loginpage.open_loginpage()
        test_loginpage.login_polly(username, password)
        pytest.assume(test_loginpage.wait_get_element_text(locator) == expected)
        if test_loginpage.driver.current_url == HOME_URL:
            # sleep(5)
            test_loginpage.click_element(['xpath', '//img'])
            # sleep(5)
            test_loginpage.wait_click_element(['xpath', "//span[text()='退出']"])
if __name__ == '__main__':
    # -k keyword关键字，函数里面的字符串模糊匹配，即指定用例执行
    pytest.main(['-sv',__file__,'--alluredir',reports_path,'--clean-alluredir'])
    os.system(f'allure serve {reports_path}')
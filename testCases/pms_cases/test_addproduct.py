#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : test_addproduct.py
@Time   : 2021/11/25 15:25
@IDE    : PyCharm
@Introduction:测试添加商品页面
"""
import pytest
import os
from PageObjects.LoginPage import LoginPage
from PageObjects.HomePage import HomePage
from utils.handle_rand_str import get_rand_str
from utils.handle_path import reports_path

def test_addprduct(init_pms):
    '''
    1.打开登录页
    2.登录宝利商城
    3.进入首页   首页页面对象
    4.点击添加商品页面   添加商品的页面对象
    5.操作步骤   添加商品的方法
    6.断言:切换到商品列表界面， 判断第一 个商品是否是刚才新增的商品   商品列表页面对象，获取第一个商品的方法

    @return:
    '''
    # test_loginpage = LoginPage()
    # test_loginpage.open_loginpage()
    # test_loginpage.login_polly("macro", "macro123456")
    # test_homepage = HomePage()
    # test_homepage.goto_addproductpage()
    # 登录放在pms下面的conftest作为初始化
    # test_homepage = LoginPage().open_loginpage().login_polly("macro", "macro123456")
    test_homepage = init_pms
    test_addprductpage = test_homepage.goto_addproductpage()
    # productname要做到不可重复，随机
    test_pname = '自动化'+get_rand_str(5)
    title_name = '副标题' + get_rand_str(5)
    test_addprductpage.addproduct('1','1',test_pname,title_name,'1')
    # 回到主页
    test_addprductpage.back_homepage()
    # 切换到商品列表
    test_productlistpage = test_homepage.goto_productlistpage()
    # 获取商品列表页面的第一个
    result_productname = test_productlistpage.get_firstproduct_name()
    # 断言
    assert test_pname == result_productname
    # 重复跑pytest_repeat()
    # test_productlistpage.logout_polly()
if __name__ == '__main__':
    pytest.main(['-sv', __file__, '--alluredir', reports_path, '--clean-alluredir'])
    os.system(f'allure serve {reports_path}')
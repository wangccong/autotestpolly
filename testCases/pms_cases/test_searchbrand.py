#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : test_searchbrand.py
@Time   : 2021/11/26 15:21
@IDE    : PyCharm
@Introduction:测试搜索品牌
"""
import random
import pytest
def test_searchbrand(init_pms):
    # 登录商城
    test_homepage = init_pms
    # 到品牌管理
    test_brandpage = test_homepage.goto_brandmanagepage()
    # 获取当前页所有品牌名
    init_brands = test_brandpage.get_curpage_allbrands()
    print(type(init_brands))
    print(init_brands)
    # 随机取一个品牌
    test_brandname = random.choice(init_brands)
    print(test_brandname)
    # 搜索品牌
    test_brandpage.search_brand(test_brandname)
    # 断言
    from time import sleep
    sleep(1)
    result_brands = test_brandpage.get_curpage_allbrands()
    print(result_brands)
    assert len(list(filter(lambda x: test_brandname in x, result_brands))) == len(result_brands)
if __name__ == '__main__':
    pytest.main(['-sv', __file__])
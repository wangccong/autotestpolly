#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : conftest.py
@Time   : 2021/11/26 13:57
@IDE    : PyCharm
@Introduction:
"""
import pytest
from PageObjects.LoginPage import LoginPage
@pytest.fixture(scope='session',autouse=False)
def init_pms():
    print('\n商品管理开始测试')
    test_homepage = LoginPage().open_loginpage().login_polly("macro", "macro123456")
    yield test_homepage
    print('\n商品管理测试结束')
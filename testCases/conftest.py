#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : conftest.py
@Time   : 2021/11/24 20:56
@IDE    : PyCharm
@Introduction:钩子函数
"""
import pytest
def pytest_collection_modifyitems(items):
    """
    测试用例收集完成时，将收集到的item的name和nodeid的中文显示在控制台上
    :return:
    """
    #items list
    for item in items:
        print(item.name)
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        print(item.nodeid)
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")
        # TODO 调整用例的顺序，以及删除某个用例
    #items.reverse()  #反转
    #items.pop()      #把最后一个用例去掉
    #在测试函数中的test_failed  test_all test_success


# 执行完用例后退出浏览器
@pytest.fixture(scope='session',autouse=True)
def init_polly():
    print('\nmall-admin-web开始测试')
    yield
    print('\nmall-admin-web测试结束')
    #退出浏览器
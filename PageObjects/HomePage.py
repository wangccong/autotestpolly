#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : HomePage.py
@Time   : 2021/11/25 15:35
@IDE    : PyCharm
@Introduction:首页
"""
from common.basepage import BasePage
from PageObjects.PmsObjects.AddProductPage import AddProductPage
from PageObjects.PmsObjects.ProductListPage import ProductListPage
from PageObjects.PmsObjects.BrandManagePage import BrandManagePage

class HomePage(BasePage):
    def goto_addproductpage(self):
        self.click_element(self.menu_productmanage)
        self.wait_click_element(self.submenu_pm_addproduct)
        return AddProductPage()

    def goto_productlistpage(self):
        self.click_element(self.menu_productmanage)
        self.wait_click_element(self.submenu_pm_productlist)
        return ProductListPage()

    def goto_brandmanagepage(self):
        self.click_element(self.menu_productmanage)
        self.wait_click_element(self.submenu_pm_brandmanage)
        return BrandManagePage()
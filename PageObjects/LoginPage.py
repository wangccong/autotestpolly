#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : LoginPage.py
@Time   : 2021/11/23 10:20
@IDE    : PyCharm
@Introduction:登录页面
"""
from common.basepage import BasePage
from config.project_config import POLLY_URL
from PageObjects.HomePage import HomePage

class LoginPage(BasePage):
    """
    1、打开登录页
    2、登录系统
    """
    def open_loginpage(self):
        self.open_url(POLLY_URL)
        return self
    def login_polly(self,username,password):
        self.input_text(self.username_input, username)
        self.input_text(self.password_input, password)
        self.click_element(self.login_button)
        return HomePage()
if __name__ == '__main__':
    test_loginpage = LoginPage()
    test_loginpage.open_loginpage()
    test_loginpage.login_polly("macro","macro123456")
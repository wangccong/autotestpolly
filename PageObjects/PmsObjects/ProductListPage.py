# Author:  wuxianfeng
# Company: songqin
# File:    ProductListPage.py
# Date:    2021/10/24
# Time:    11:06
from common.basepage import BasePage


class ProductListPage(BasePage):
    def get_firstproduct_name(self):
        return self.get_element_text(self.first_product)

    def logout_polly(self):
        self.wait_click_element(self.personal_button)
        self.wait_click_element(self.logout_button)

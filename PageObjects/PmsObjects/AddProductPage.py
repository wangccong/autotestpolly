#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : AddProductPage.py
@Time   : 2021/11/25 16:25
@IDE    : PyCharm
@Introduction:添加商品页面
"""
from common.basepage import BasePage
class AddProductPage(BasePage):
    def back_homepage(self):
        # click_element()调为等待后切换
        self.wait_click_element(self.home_button)
    def addproduct(self,kidx1,kidx2,pname,subtitle,bidx):
        # 1.点击商品分类
        self.click_element(self.product_kind_select)
        # 2.选择1级分类
        self.product_kind_select_index1[-1] = self.product_kind_select_index1[-1].format(kidx1)
        self.click_element(self.product_kind_select_index1)
        # 3.选择2级分类
        self.product_kind_select_index2[-1] = self.product_kind_select_index2[-1].format(kidx2)
        self.click_element(self.product_kind_select_index2)
        # 4.输入商品名称
        self.input_text(self.product_name, pname)
        # 5.输入副标题
        self.input_text(self.product_subtitle, subtitle)
        # 6.点击商品品牌
        self.click_element(self.product_brand_select)
        # 7.选择品牌1级分类
        self.product_brand_select_idx[-1] = self.product_brand_select_idx[-1].format(bidx)
        self.wait_click_element(self.product_brand_select_idx)
        # 8. 下一步，填写商品促销
        self.wait_click_element(self.next_commodity_promotion_btn)
        # 9. 下一步，填写商品属性
        self.wait_click_element(self.next_product_attribute_btn)
        # 10. 下一步，选择商品关联
        self.wait_click_element(self.netxt_product_related_btn)
        # 11. 完成，提交商品
        self.wait_click_element(self.complete_btn)
        # 12. 确定
        self.wait_click_element(self.submit_btn)
#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
@Version: v1.0
@Project: AutoTestPolly
@Author : Verni
@File   : BrandManagePage.py
@Time   : 2021/11/26 15:34
@IDE    : PyCharm
@Introduction:品牌管理页面
"""
from common.basepage import BasePage

class BrandManagePage(BasePage):
    # 获取当前页的所有品牌信息
    def get_curpage_allbrands(self):
        return self.get_elements(self.all_brand_name_txt)

    def search_brand(self, brandname):
        self.input_text(self.brand_search_input, brandname)
        self.wait_click_element(self.search_btn)